import 'package:audio_player_app/pages/home_page.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Flutter Demo',
      theme: ThemeData(
        colorScheme: ColorScheme.fromSeed(seedColor: Colors.deepPurple),
        useMaterial3: true,
      ),
      home: const HomePage(),
    );
  }
}
/*
  Packages Used :-
    1] flutter pub add just_audio ===> gives functionality of playing audio
    2] flutter pub add audio_video_progress_bar


  Enable multidex.

    Open project/app/build.gradle and add the following lines.

defaultConfig {
    ...

    multiDexEnabled true
}
and

dependencies {
    ...

    implementation 'com.android.support:multidex:1.0.3'
}
*/
